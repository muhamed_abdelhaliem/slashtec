#! /bin/bash
# Build All Images
cd airports-assembly-1.0.1 && docker build -t  airports-assembly-1.0.1 .
cd .. 
cd airports-assembly-1.1.0 && docker build -t airports-assembly-1.1.0 .
cd .. 
cd countries-assembly-1.0.1 && docker build -t countries-assembly .
# Apply All  Deployment 
cd ..
kubectl create ns slashtec
kubectl apply -f airports-assembly-1.0.1/airports-1.0.1.yaml
kubectl apply -f airports-assembly-1.1.0/airoports-1.1.0.yaml
kubectl apply -f countries-assembly-1.0.1/countries-assembly.yaml
