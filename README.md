## What we'll build
- Java applications with Kubernetes and docker-compose
## What you'll need
- Docker CE version + 18.06
- Kubernetes version + 1.12
- docker-compose version + 1.18
## RUN
```shell
git clone https://bitbucket.org/muhamed_abdelhaliem/slashtec.git
cd slashtec/
```
## Using docker-compose
Service airports-1.0.1 
```shell
cd airports-assembly-1.0.1/
docker-compose up -d
```
The service would up on port 8000
- Access to http://localhost/8000
or
- Access to http://yourIP/8000

Service airports-1.1.0
```shell
cd airports-assembly-1.1.0
docker-compose up -d
```
The service would up op port 8001
- Access to http://localhost/8001
or
- Access to http://yourIP/8001

Service countries-assembly-1.0.1
```shell
cd airports-assembly-1.1.0
docker-compose up -d
```
The service would up op port 8002
- Access to http://localhost/8002
or
- Access to http://yourIP/8002
## Using Kubernetes
deploy all services by single command 
```shell
./init.sh
```
namespace slashtec will created
deployments will be created
```
./init.sh
Sending build context to Docker daemon  37.51MB
Step 1/3 : FROM adoptopenjdk/openjdk8-openj9
 ---> 8121b100148b
Step 2/3 : COPY airports-assembly-1.0.1.jar   airports-assembly-1.0.1.jar
 ---> Using cache
 ---> fb4791d2b612
Step 3/3 : ENTRYPOINT ["java","-jar","/airports-assembly-1.0.1.jar"]
 ---> Using cache
 ---> f307ebe9d802
Successfully built f307ebe9d802
Successfully tagged airports-assembly-1.0.1:latest
Sending build context to Docker daemon  37.51MB
Step 1/3 : FROM adoptopenjdk/openjdk8-openj9
 ---> 8121b100148b
Step 2/3 : COPY airports-assembly-1.1.0.jar  airports-assembly-1.1.0.jar
 ---> Using cache
 ---> 3e8dca7eaf70
Step 3/3 : ENTRYPOINT ["java","-jar","/airports-assembly-1.1.0.jar"]
 ---> Using cache
 ---> af77720b7bf8
Successfully built af77720b7bf8
Successfully tagged airports-assembly-1.1.0:latest
Sending build context to Docker daemon  34.32MB
Step 1/3 : FROM adoptopenjdk/openjdk8-openj9
 ---> 8121b100148b
Step 2/3 : COPY countries-assembly-1.0.1.jar  countries-assembly-1.0.1.jar
 ---> Using cache
 ---> 6dc7c9bf04df
Step 3/3 : ENTRYPOINT ["java","-jar","/countries-assembly-1.0.1.jar"]
 ---> Using cache
 ---> c59196d3974c
Successfully built c59196d3974c
Successfully tagged countries-assembly:latest
namespace/slashtec created
deployment.apps/airports-assembly-1-0-1 created
service/airports-assembly-1-0-1 created
deployment.apps/airports-assembly-1-1-0 created
service/airports-assembly-1-1-0 created
deployment.apps/countries-assembly created
```

```
kubectl get deployment -n slashtec
NAME                    DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
airports-assembly-1-0   1         1         1            1           75m
airports-assembly-1-1   1         1         1            1           75m
countries-assembly      1         1         1            1           75m
```
Services will be created 
```
kubectl get svc -n slashtec
NAME                    TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
airports-assembly-1-0   LoadBalancer   10.97.190.139   <pending>     8080:30120/TCP   78m
airports-assembly-1-1   LoadBalancer   10.99.86.135    <pending>     8080:30121/TCP   78m
countries-assembly      LoadBalancer   10.96.183.205   <pending>     8080:30122/TCP   78m
```
##  Deploy Using Jenkins 
create pipeline 
change repo credentails with your credentials in Jenkins file
Script Path Jenkinsfile 
